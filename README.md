# Spotify API module.

### Description

This module allows to make a connection to Spotify API and returns Spotify 
artists data.

### Requirements

- Drupal 8.9.x

### Installation

Install this module as you would normally install a Drupal module. 
Visit https://www.drupal.org/node/1897420 for further information.

By using DRUSH:
drush en webdevfreak_spotify

### Configuration

Log into your Spotify account and go to dashboard. Copy 'Client ID' & 
'Client Secret' values.

Go to admin/config/spotify

Paste 'Client ID' & 'Client Secret' values in the textboxes.

'No of artists to display' field should have any integer value less than 20.
