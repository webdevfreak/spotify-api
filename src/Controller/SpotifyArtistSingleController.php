<?php
/**
 * @file
 * Contains \Drupal\webdevfreak_spotify\Controller\SpotifyArtistSingleController.
 */

namespace Drupal\webdevfreak_spotify\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Xss;

/**
 * Class SpotifyArtistSingleController.
 *
 * @package Drupal\webdevfreak_spotify\Controller
 */
class SpotifyArtistSingleController extends ControllerBase {

  /**
   * Get single artist data.
   * 
   * @see https://developer.spotify.com/documentation/web-api/reference/artists/get-artist/
   *
   * @return string
   *   Single artist data.
   */
  public function webdevfreak_spotify_artist_single(){
    // Initialize variable.
    $artist_data = '';

    // Get id from url.
    $spotify_artist_id = Xss::filter(webdevfreak_spotify_get_id_from_url(3));
    
    // Session.
    $tempstore = \Drupal::service('user.private_tempstore')->get('webdevfreak_spotify');

    // Spotify API endpoint.
    $endpoint  = 'https://api.spotify.com/v1/artists/' . $spotify_artist_id;
    
    // Spotify API request options.
    $options = [
      'headers' => [
        'Authorization' => 'Bearer ' . $tempstore->get('access_token'),
      ],
    ];

    // Use try / catch to request artist data from Spotify.
    try {
      // Make API request.
      $client = \Drupal::httpClient();
      $request = $client->request('GET', $endpoint, $options);

      // If success then execute this block.
      if ($request->getStatusCode() == 200) {
      	// Get JSON decode data in a variable.
        $body = json_decode($request->getBody()->getContents());

        // Get artist data in a variable.
        $artist_data .= $this->t('Name: ' . $body->name . '<br>');        
        $artist_data .= $this->t('Spotify URL: <a href="@spotify-url">' . 
          $body->external_urls->spotify . '</a><br>', ['@spotify-url' => 
          $body->external_urls->spotify]);
        $artist_data .= $this->t('ID: ' . $body->id . '<br>');
      }
    }
    catch (RequestException $e){
      // Log the error.
      watchdog_exception('webdevfreak_spotify', $e);
    }

    return ['#markup' => $artist_data];
  }

}
