<?php

namespace Drupal\webdevfreak_spotify\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SpotifyConfig.
 *
 * @package Drupal\webdevfreak_spotify\Form
 */
class SpotifySettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'webdevfreak_spotify.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webdevfreak_spotify_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('webdevfreak_spotify.settings');
    
    $form['apikey_live'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Spotify credentials'),
      '#description' => $live_test_description,
    ];

    $form['apikey_live']['apikey_client_live'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => $config->get('apikey_client_live'),
      '#required' => TRUE,
    ];

    $form['apikey_live']['apikey_secret_live'] = [
      // '#type' => 'password',
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#default_value' => $config->get('apikey_secret_live'),
      '#required' => TRUE,
    ];

    $form['apikey_live']['apikey_artist_no'] = [
      '#type' => 'textfield',
      '#title' => $this->t('No of artists to display'),
      '#default_value' => $config->get('apikey_artist_no'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('apikey_artist_no') > 20) {
      $form_state->setErrorByName('apikey_artist_no', $this->t('This value 
        can not be higher than 20.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('webdevfreak_spotify.settings');

    $this->config('webdevfreak_spotify.settings')
      ->set('apikey_client_live', $form_state->getValue('apikey_client_live'))
      ->set('apikey_secret_live', $form_state->getValue('apikey_secret_live'))
      ->set('apikey_artist_no', $form_state->getValue('apikey_artist_no'))
      ->save();

      parent::submitForm($form, $form_state);
  }

}
