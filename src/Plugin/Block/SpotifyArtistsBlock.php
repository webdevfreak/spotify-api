<?php
/**
 * @file
 * Contains \Drupal\webdevfreak_spotify\Plugin\Block\SpotifyArtistsBlock.
 */

namespace Drupal\webdevfreak_spotify\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a Spotify Artists Block.
 *
 * @Block(
 *   id = "spotify_artists_block",
 *   admin_label = @Translation("Spotify Artists Block"),
 *   category = @Translation("Spotify Artists"),
 * )
 */
class SpotifyArtistsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Initialise variables.
    $access_token = NULL;
    $artist_data = NULL;

    // Get Spotify API access token.
    $access_token = webdevfreak_spotify_api_connection();

    // Get an Artist's Related Artists.
    if($access_token != NULL){
      $artist_data = webdevfreak_spotify_api_get_artists($access_token);
    }

    return ['#markup' => $artist_data];
  }

}
